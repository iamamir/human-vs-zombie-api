package com.hvz.filters;

import com.hvz.models.User;
import com.hvz.models.CurrentUser;
import com.hvz.security.JwtUtil;
import com.hvz.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtRequestFilter extends OncePerRequestFilter{

    private User dbUser = null;
    private final UserService userService = new UserService();
    private final JwtUtil jwtUtil = new JwtUtil();
    private CurrentUser currentUser;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String authorizationHeader = httpServletRequest.getHeader("Authorization");

        String email = null;
        String jwt = null;

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            jwt = authorizationHeader.substring(7);
            email = jwtUtil.extractEmail(jwt);
        }

        if(email != null){
            dbUser = userService.getUserByEmail(email).getBody();

            // TODO - ASK IF THIS ERROR IS OKAY
            // Creates an error if the key has user key that is deleted

            if(jwtUtil.validateToken(jwt,dbUser)){

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(dbUser,null,null);
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

                currentUser = CurrentUser.getInstance();
                currentUser.setEmail(email);

            }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
