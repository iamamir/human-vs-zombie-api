package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Game;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GameDB {
    private final DBConnection dbConnection = new DBConnection();
    private final Logger logger = new Logger();

    public List<Game> getAllGames() {
        List<Game> games = new ArrayList<>();
        String query = "select * from game";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                games.add(new Game(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                ));
            }

            logger.log("Get list of games");

        } catch (Exception e) {
            logger.log(e.toString());
        }
        return games;
    }

    public Game getGame(String gameId) {
        Game game = null;
        String query = "SELECT * FROM game WHERE game_id = ?";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, gameId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                game = new Game(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
            }
            logger.log("Get a specific game");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return game;
    }

    public Boolean addGame(Game game) {
        boolean status = true;
        String query = "INSERT into game ( game_name, nw_latitude, nw_longitude, se_latitude, se_longitude) values (?, ?, ?, ?, ?)";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, game.getGame_name());
            ps.setString(2, game.getNw_latitude());
            ps.setString(3, game.getNw_longitude());
            ps.setString(4, game.getSe_latitude());
            ps.setString(5, game.getSe_longitude());

            ps.executeUpdate();
            logger.log("Add game");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean updateGame(Game game) {
        boolean status = true;
        String query = "UPDATE game set game_name = ?, game_state = ?, nw_latitude=?, nw_longitude = ?, se_latitude = ?,se_longitude= ? where game_id = ?";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, game.getGame_name());
            ps.setString(2, game.getGame_state());
            ps.setString(3, game.getNw_latitude());
            ps.setString(4, game.getNw_longitude());
            ps.setString(5, game.getSe_latitude());
            ps.setString(6, game.getSe_longitude());
            ps.setString(7, game.getId());

            ps.executeUpdate();
            logger.log("Update game");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean deleteGame(String gameId) {
        boolean status = true;
        String query = "DELETE FROM game where game_id = ?";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, gameId);
            ps.executeUpdate();

            logger.log("Deleted game successful");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean gameExist(String gameId) {
        String query = "SELECT count(*) FROM game WHERE game_id = ?";
        int count = 0;

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, gameId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Checking if game exist");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }

}
