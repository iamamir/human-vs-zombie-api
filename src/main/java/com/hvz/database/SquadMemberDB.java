package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Squad;
import com.hvz.models.SquadMember;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SquadMemberDB {
    private final DBConnection dbConnection = new DBConnection();
    private final Logger logger = new Logger();

    public ResponseEntity<ArrayList<SquadMember>> getAllSquadMembers(String gameId, String squadId) {

        ArrayList<SquadMember> squadMembers = new ArrayList<>();
        String query = "SELECT * FROM squad_member where squad_id = ? and game_id = ?;";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, gameId);
            ps.setString(2, squadId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                squadMembers.add(new SquadMember(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }

            logger.log("Returned a list of squad members");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(squadMembers, HttpStatus.OK);
    }

    public ResponseEntity<SquadMember> addSquadMember(String game_id, String squad_id, String player_id) {

        try (PreparedStatement ps = dbConnection.conn().prepareStatement("INSERT INTO squad_member (game_id, squad_id, player_id) values (?, ?, ?)")) {
            ps.setString(1, game_id);
            ps.setString(2, squad_id);
            ps.setString(3, player_id);

            ps.executeUpdate();

            logger.log("Added squad member successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public ResponseEntity<SquadMember> setSquadLeader(String game_id, String squad_id, String player_id){

        try (PreparedStatement ps = dbConnection.conn().prepareStatement("INSERT INTO squad_member (game_id, squad_id, player_id, rank_) values (?, ?, ?,?)")) {
            ps.setString(1, game_id);
            ps.setString(2, squad_id);
            ps.setString(3, player_id);
            ps.setString(4,"Leader");

            ps.executeUpdate();

            logger.log("Added squad leader successfully");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);


    }

    public boolean isMember(String game_id, String squad_id, String player_id) {
        String query = "SELECT count(*) FROM squad_member WHERE game_id = ? and squad_id = ? and player_id = ?; ";
        int count = 0;

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, squad_id);
            ps.setString(3, player_id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count >= 1;
    }


}
