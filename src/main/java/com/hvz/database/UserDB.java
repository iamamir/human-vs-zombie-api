package com.hvz.database;

import com.hvz.helpers.PasswordHelper;
import com.hvz.logger.Logger;
import com.hvz.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDB {

    private final DBConnection dbConn = new DBConnection();
    private final Logger logger = new Logger();

    private User dbUser = null;
    private final PasswordHelper passwordHelper = new PasswordHelper();

    public List<User> getAllUsers() {

        List<User> users = new ArrayList<>();

        try (PreparedStatement ps = dbConn.conn().prepareStatement("Select user_id, email, password, salt, is_administrator FROM user");) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                users.add(new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }

            logger.log("Get a list of users");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public User getUserById(String user_id) {

        try (PreparedStatement ps = dbConn.conn().prepareStatement("Select user_id,email, password, salt, is_administrator FROM user where user_id = ?")) {
            ps.setString(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbUser = new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)

                );
            }
            logger.log("Get specific user by id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbUser;
    }

    // Select user by email
    public User getUserByEmail(String email) {

        try (PreparedStatement ps = dbConn.conn().prepareStatement("Select  user_id,email, password, salt, is_administrator FROM user where email = ?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbUser = new User(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                );
            }
            logger.log("Get specific user by email");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbUser;
    }

    // Insert new user
    public boolean addUser(String email, String password) {
        boolean status = true;

        try (PreparedStatement ps = dbConn.conn().prepareStatement("insert into user(email,salt,password) values(?,?,?)")) {

            String salt = passwordHelper.generateSalt();
            String hashedPassword = passwordHelper.hash(password, salt);

            ps.setString(1, email);
            ps.setString(2, salt);
            ps.setString(3, hashedPassword);
            ps.executeUpdate();

            logger.log("Adding a new user ");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }

        return status;
    }

    // Update user
    public boolean updateUser(User user) {
        boolean status = true;
        try (PreparedStatement ps = dbConn.conn().prepareStatement("UPDATE user SET email = ? WHERE user_id = ?")) {
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getId());
            ps.executeUpdate();

            logger.log("Updating a specific user");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    // Delete user
    public boolean deleteUser(String email) {
        boolean status = true;

        try (PreparedStatement ps = dbConn.conn().prepareStatement("DELETE FROM user WHERE email = ?")) {
            ps.setString(1, email);
            ps.executeUpdate();

            logger.log("Deleting a specific user by email");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean checkIfUserExistById(String user_id) {
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement("SELECT count(*) FROM user WHERE user_id =?")) {
            ps.setString(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if user exist by id");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count >= 1;
    }

    public boolean checkIfUserExistEmail(String email) {
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement("SELECT count(*) FROM user WHERE email =?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if user exist by email");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }

    public boolean checkIsAdmin(String email) {
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement("SELECT count(*) FROM user WHERE email = ? and is_administrator = 1")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if user is admin");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }
}
