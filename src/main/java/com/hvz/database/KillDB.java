package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Kill;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KillDB {

    private final DBConnection dbConn = new DBConnection();

    private final Logger logger = new Logger();

    public List<Kill> getAllKills(String game_id) {
        List<Kill> kills = new ArrayList<>();
        String query = "Select * from kill_ where game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {

            ps.setString(1, game_id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                kills.add(new Kill(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                ));
            }
            logger.log("Get list of kills");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kills;
    }

    public Kill getKill(String game_id, String kill_id) {
        Kill kill = null;
        String query = "Select * from kill_ where game_id = ? and kill_id = ?";

        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, kill_id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                kill = new Kill(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                );
            }

            logger.log("Get specific kill");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kill;
    }

    public boolean addKill(Kill kill) {
        boolean status = true;
        String query = "Insert into kill_(story,latitude,longitude,game_id,killer_id,victim_id) values(?,?,?,?,?,?)";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, kill.getStory());
            ps.setString(2, kill.getLatitude());
            ps.setString(3, kill.getLongitude());
            ps.setString(4, kill.getGameId());
            ps.setString(5, kill.getKillerId());
            ps.setString(6, kill.getVictimId());
            ps.executeUpdate();

            logger.log("Add kill");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean updateKill(Kill kill) {
        boolean status = true;
        String query = "UPDATE kill_ SET time_of_death = ?, story = ?, latitude = ?, longitude = ? WHERE kill_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, kill.getTimeOfDeath());
            ps.setString(2, kill.getStory());
            ps.setString(3, kill.getLatitude());
            ps.setString(4, kill.getLongitude());
            ps.setString(5, kill.getId());
            ps.executeUpdate();

            logger.log("Update kill");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean deleteKill(String game_id, String kill_id) {
        boolean status = true;
        String query = "DELETE FROM kill_ WHERE game_id = ? and kill_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, kill_id);
            ps.executeUpdate();

            logger.log("Delete kill");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }

        return status;
    }

    public boolean checkIfKillExist(String kill_id, String game_id) {
        int count = 0;
        String query = "SELECT count(*) FROM kill_ WHERE kill_id = ? and game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, kill_id);
            ps.setString(2, game_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if kill exist");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }


}
