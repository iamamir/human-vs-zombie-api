package com.hvz.database;

import java.sql.*;


public class DBConnection {

    public Connection conn(){

        String url = "jdbc:mysql://localhost:3306/hvz?user=root&password=root";
        //String url = "jdbc:mysql://eu-cdbr-west-03.cleardb.net:3306/heroku_030a2f10b7674c6?user=bd4ae28df2ab17&password=35a8f47a";
        try {
            return  DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
}
