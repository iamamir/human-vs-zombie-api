package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MessagesDB {
    /*
    private final DBConnection dbConnection = new DBConnection();
    private final Logger logger = new Logger();

    public ResponseEntity<ArrayList<Message>> getGameMessages(String game_id, String squad_id, String is_human_global, String is_zombie_global) {
        ArrayList<Message> messages = new ArrayList<>();

        String query = "SELECT * FROM message where game_id = ? and is_human_global = ? and is_zombie_gobal = ? and squad_id = ?";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, game_id);
            ps.setString(2, is_human_global);
            ps.setString(3, is_zombie_global);
            ps.setString(4, squad_id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                messages.add(new Message(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                ));
            }

            logger.log("Get game global messages successful");
        } catch (SQLException e) {
            logger.log(e.toString());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    public ResponseEntity<Message> addMessage(Message message) {

        String query = "INSERT into message ( message_body , is_human_global, is_zombie_gobal,game_id,player_id,squad_id) values (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, message.getMessageBody());
            ps.setString(2, message.getIsHumanGlobal());
            ps.setString(3, message.getIsZombieGlobal());
            ps.setString(4, message.getGameId());
            ps.setString(5, message.getPlayerId());
            ps.setString(6, message.getSquadId());

            ps.executeUpdate();
            logger.log("Added message successfully");
        } catch (SQLException e) {
            logger.log(e.toString());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }




    public List<Message> getGlobalGameMessages(String gameId, String type) {
        String query;
        List<Message> messages = new ArrayList<>();
        switch (type) {
            case "global-human":
                messages = getGameMessages(gameId, null, "1", "0");
                break;
            case "global-zombie":
                messages = getGameMessages(gameId, null, "0", "1");
                break;
            case "global-human-zombie":
                messages = getGameMessages(gameId, null, "1", "1");
                break;
        }
        return messages;
    }


    public ResponseEntity<ArrayList<Message>> getSquadMessages(String game_id, String squad_id) {
        return getGameMessages(game_id, squad_id, "0", "0");
    }
    */
}
