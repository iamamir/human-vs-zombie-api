package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Player;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlayerDB {

    private final DBConnection dbConn = new DBConnection();
    private Player dbPlayer = null;
    private final Logger logger = new Logger();

    public List<Player> getAllPlayers(String game_id) {
        List<Player> players = new ArrayList<>();
        String query = "Select player_id ,user_name, game_id, user_id from player where game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                rs.getString(2);
                rs.getString(3);
                players.add(new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)
                ));
            }
            logger.log("Get all players - no sensitive data");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return players;
    }

    public List<Player> getAllPlayersAsAdmin(String game_id) {
        List<Player> players = new ArrayList<>();
        String query = "Select * from player where game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                players.add(new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                ));
            }
            logger.log("Get all players - admin");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return players;
    }

    public Player getPlayer(String game_id, String player_id) {
        String query = "Select user_name, is_human, bite_code, game_id,user_id from player where game_id = ? and player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, player_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbPlayer = new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)

                );
            }
            logger.log("Get specific player");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbPlayer;
    }

    public Player getPlayerAsAdmin(String game_id, String player_id) {

        String query = "Select * from player where game_id = ? and player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, player_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbPlayer = new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                );
            }
            logger.log("Get specific player - admin");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbPlayer;
    }

    public Player getPlayerByBiteCode(String bite_code, String game_id) {
        String query = "Select * from player where bite_code = ? and game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {

            ps.setString(1, bite_code);
            ps.setString(2, game_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbPlayer = new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                );
            }
            logger.log("Get specific player by bitecode");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbPlayer;
    }

    public Player getPlayerByUserId(String user_id, String game_id) {
        String query = "Select * from player where user_id = ? and game_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, user_id);
            ps.setString(2, game_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbPlayer = new Player(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7)
                );
            }
            logger.log("Get specific player by userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbPlayer;
    }

    public boolean addPlayer(String game_id, Player player, String bite_code) {
        boolean status = true;
        String query = "insert into player(user_name,bite_code,user_id,game_id) values(?,?,?,?)";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, player.getUserName());
            ps.setString(2, bite_code);
            ps.setString(3, player.getUserId());
            ps.setString(4, game_id);
            ps.executeUpdate();

            logger.log("Add player");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }

        return status;
    }

    public boolean updatePlayer(Player player) {
        boolean status = true;
        String query = "UPDATE player SET user_name = ?, bite_code = ?, is_human = ?, is_patient_zero = ? WHERE player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, player.getUserName());
            ps.setString(2, player.getBiteCode());
            ps.setString(3, player.getIs_human());
            ps.setString(4, player.getIs_patient_zero());
            ps.setString(5, player.getId());

            ps.executeUpdate();
            logger.log("Update player");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean deletePlayer(String game_id, String player_id) {
        boolean status = true;
        String query = "DELETE FROM player WHERE game_id = ? and player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, player_id);

            ps.executeUpdate();
            logger.log("Delete player");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean playerExist(String player_id, String game_id) {
        String query = "SELECT count(*) FROM player WHERE game_id = ? and player_id = ?";
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, player_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if player exist by playerId");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }

    public boolean userNameExistInGame(String game_id, String username) {
        String query = "SELECT count(*) FROM player WHERE game_id = ? and user_name = ?";
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, game_id);
            ps.setString(2, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if username exists in the game");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }

    public boolean playerExistByUserId(String user_id, String game_id) {
        String query = "SELECT count(*) FROM player WHERE user_id = ? and game_id = ?";
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, user_id);
            ps.setString(2, game_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if player exist by userId");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;


    }

    public boolean biteCodeExist(String bite_code, String game_id) {
        String query = "SELECT count(*) FROM player WHERE bite_code = ? and game_id = ?";
        int count = 0;
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, bite_code);
            ps.setString(2, game_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if biteCode exists");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count >= 1;
    }


    public boolean killPlayer(String player_id) {
        boolean status = true;
        String query = "UPDATE player SET is_human = 0 WHERE player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, player_id);
            ps.executeUpdate();
            logger.log("Killing the player in playerDB (updating is_human to 0)");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean revivePlayer(String player_id) {
        boolean status = true;
        String query = "UPDATE player SET is_human = 1 WHERE player_id = ?";
        try (PreparedStatement ps = dbConn.conn().prepareStatement(query)) {
            ps.setString(1, player_id);
            ps.executeUpdate();
            logger.log("reviving the player in playerDB (updating is_human to 1)");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }


}
