package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Mission;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MissionDB {

    private final DBConnection dbConnection = new DBConnection();
    private final Logger logger = new Logger();


    public List<Mission> getGameMissionsFactionSpecific(String gameId, String isHuman, String isZombie) {
        List<Mission> missions = new ArrayList<>();
        String query = "SELECT * FROM mission where game_id = ? and is_human_visible = ? and is_zombie_visible = ? ;";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, gameId);
            ps.setString(2, isHuman);
            ps.setString(3, isZombie);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                missions.add(new Mission(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }

            logger.log("Get list of missions");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return missions;
    }

    public List<Mission> getGameMissions(String gameId) {
        List<Mission> missions = new ArrayList<>();
        String query = "SELECT * FROM mission where game_id = ? ;";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, gameId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                missions.add(new Mission(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                        ));
            }

            logger.log("Get mission");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return missions;
    }

    public Mission getGameMission(String gameId, String missionId) {
        Mission mission = null;
        String query = "SELECT * FROM mission where game_id = ? and mission_id = ?;";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, gameId);
            ps.setString(2, missionId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                mission = new Mission(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                );
            }
            logger.log("Get mission");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mission;
    }

    public boolean addMission(Mission mission) {
        boolean status = true;
        String query = "insert into mission (mission_name, is_human_visible, is_zombie_visible, description, start_time, end_time, game_id, latitude, longitude) values(?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, mission.getMission_name());
            ps.setString(2, mission.getIs_human_visible());
            ps.setString(3, mission.getIs_zombie_visible());
            ps.setString(4, mission.getDescription());
            ps.setString(5, mission.getStart_time());
            ps.setString(6, mission.getEnd_time());
            ps.setString(7, mission.getGame_id());
            ps.setString(8, mission.getLatitude());
            ps.setString(9,mission.getLongitude());


            ps.executeUpdate();

            logger.log("Add mission");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;

        }
        return status;

    }

    public boolean updateMission(String gameId, String missionId, Mission mission) {
        boolean status = true;
        String query = "update mission set mission_name = ?, is_human_visible = ?, is_zombie_visible = ?, description = ?, start_time = ? , end_time = ? where  game_id = ? and mission_id = ?;";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, mission.getMission_name());
            ps.setString(2, mission.getIs_human_visible());
            ps.setString(3, mission.getIs_zombie_visible());
            ps.setString(4, mission.getDescription());
            ps.setString(5, mission.getStart_time());
            ps.setString(6, mission.getEnd_time());
            ps.setString(7, gameId);
            ps.setString(8, missionId);

            ps.executeUpdate();
            logger.log("Update mission");
        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean deleteMission(String gameId, String missionId) {
        boolean status = true;
        String query = "DELETE FROM mission where game_id = ? and mission_id = ?;";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, gameId);
            ps.setString(2, missionId);

            ps.executeUpdate();
            logger.log("Delete mission");

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public boolean isMissionExist(String gameId, String missionId) {
        String query = "SELECT count(*) FROM mission WHERE game_id = ? and mission_id = ?;";
        int count = 0;

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, gameId);
            ps.setString(2, missionId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            logger.log("Check if mission exist");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count >= 1;
    }


}
