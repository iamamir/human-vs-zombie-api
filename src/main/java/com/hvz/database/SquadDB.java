package com.hvz.database;

import com.hvz.logger.Logger;
import com.hvz.models.Squad;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SquadDB {

    private final DBConnection dbConnection = new DBConnection();

    private final Logger logger = new Logger();

    public ResponseEntity<ArrayList<Squad>> getAllGameSquad(String game_id) {

        ArrayList<Squad> squads = new ArrayList<>();
        try (PreparedStatement ps = dbConnection.conn().prepareStatement("SELECT * FROM squad where game_id = ?")) {
            ps.setString(1, game_id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                squads.add(new Squad(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)

                ));
            }

            logger.log("Returned a list of squads");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(squads, HttpStatus.OK);
    }

    public ResponseEntity<Squad> getGameSquad(String gameId, String squadId) {
        Squad squad = null;

        try (PreparedStatement ps = dbConnection.conn().prepareStatement("SELECT * FROM squad where game_id = ? and squad_id = ?")) {
            ps.setString(1, gameId);
            ps.setString(2, squadId);

            ResultSet rs = ps.executeQuery();
            rs.next();
            squad = new Squad(rs.getString(1), rs.getString(2), rs.getString(3),
                    rs.getString(4));

            logger.log("Returned a squad");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(squad, HttpStatus.OK);
    }


    public ResponseEntity<Squad> addSquad(Squad squad) {

        String query = "insert into squad (sqaud_name, is_human, game_id) values(?, ?, ?);";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, squad.getSquadName());
            ps.setString(2, squad.getIsHuman());
            ps.setString(3, squad.getGameId());

            ps.executeUpdate();
            logger.log("Squad was added");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public ResponseEntity<Squad> updateSquad(String game_id, String squad_id, String squad_name) {

        String query = "update squad set sqaud_name = ? where  game_id = ? and squad_id = ?;";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, squad_name);
            ps.setString(2, game_id);
            ps.setString(3, squad_id);

            ps.executeUpdate();
            logger.log("Squad was updated");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public ResponseEntity<Squad> deleteSquad(String gameId, String squadId) {

        String query = "DELETE FROM squad where game_id = ? and squad_id = ?;";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, gameId);
            ps.setString(2, squadId);

            ps.executeUpdate();

            logger.log("Squad was deleted");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public boolean squadExist(String game_id, String squad_id) {
        logger.log("Checking if sqaud with id : " + squad_id +" exist in game with id :" +game_id);
        String query = "SELECT count(*) FROM squad WHERE game_id = ? and squad_id = ?;";
        int count = 0;
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, game_id);
            ps.setString(2, squad_id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count >= 1;
    }


}
