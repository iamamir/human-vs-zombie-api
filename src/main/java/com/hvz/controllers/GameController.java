package com.hvz.controllers;
import com.hvz.models.Game;
import com.hvz.services.GameService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/game")
public class GameController {

    private final GameService gameService = new GameService();

    // @RequestHeader (name="Authorization") String token Can be done
    @GetMapping()
    public ResponseEntity<List<Game>> getAllGames() {
        return gameService.getAllGames();
    }

    @PostMapping()
    public ResponseEntity<Game> addGame(@RequestBody Game game) {
        return gameService.addGame(game);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<Game> getGame(@PathVariable String gameId) {
        return gameService.getGame(gameId);
    }

    @PutMapping("/{gameId}")
    public ResponseEntity<Game> updateGame(@PathVariable String gameId, @RequestBody Game game) {
        return gameService.updateGame(gameId, game);
    }
    @DeleteMapping("/{gameId}")
    public ResponseEntity<Game> deleteGame(@PathVariable String gameId) {
        return gameService.deleteGame(gameId);
    }

}
