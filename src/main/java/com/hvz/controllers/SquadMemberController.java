package com.hvz.controllers;

import com.hvz.models.SquadMember;
import com.hvz.services.SquadMemberService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@RequestMapping("api/v1/game/{gameId}/squad/{squadId}")
public class SquadMemberController {
    private final SquadMemberService squadMemberService = new SquadMemberService();

    @GetMapping("/member")
    public ResponseEntity<ArrayList<SquadMember>> getAllGameMissions(@PathVariable String gameId, @PathVariable String squadId) {
        return squadMemberService.getAllSquadMembers(gameId, squadId);
    }

    @PostMapping(("/member"))
    public ResponseEntity<SquadMember> addSquadMember(@PathVariable String gameId, @PathVariable String squadId) {
        return squadMemberService.addSquadMember(gameId, squadId);
    }
}
