package com.hvz.controllers;

import com.hvz.models.Kill;
import com.hvz.services.KillService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/game/{game_id}/kill")
public class KillController {

    KillService killService = new KillService();

    @GetMapping()
    public ResponseEntity<List<Kill>> getAllKills(@PathVariable String game_id) {
        return killService.getAllKills(game_id);
    }

    @GetMapping("/{kill_id}")
    public ResponseEntity<Kill> getKill(@PathVariable String game_id, @PathVariable String kill_id){
        return killService.getKill(game_id, kill_id);
    }

    @PostMapping("/{bite_code}")
    public ResponseEntity<Kill> addKill(@PathVariable String game_id, @PathVariable String bite_code, @RequestBody Kill kill){
        return killService.addKill(bite_code, game_id, kill);
    }

    @PutMapping("/{kill_id}")
    public ResponseEntity<Kill> updateKill(@PathVariable String game_id, @PathVariable String kill_id, @RequestBody Kill kill){
        return killService.updateKill(game_id,kill_id,kill);
    }

    @DeleteMapping("/{kill_id}")
    public ResponseEntity<Kill> deleteKill(@PathVariable String game_id, @PathVariable String kill_id){
        return killService.deleteKill(game_id,kill_id);
    }
}
