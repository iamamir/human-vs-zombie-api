package com.hvz.controllers;

import com.hvz.models.AuthenticationResponse;
import com.hvz.models.User;
import com.hvz.services.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/login")
public class AuthenticationController {
    AuthenticationService authenticationService = new AuthenticationService();

    @PostMapping()
    public ResponseEntity<AuthenticationResponse> createToken(@RequestBody User user){
        return authenticationService.requestLogin(user);
    }


}
