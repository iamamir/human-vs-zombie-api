package com.hvz.controllers;

import com.hvz.models.Squad;
import com.hvz.services.SquadService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/game/{gameId}/squad")
public class SquadController {
    private final SquadService squadService = new SquadService();

    @GetMapping()
    public ResponseEntity<ArrayList<Squad>> getAllGameSquads(@PathVariable String gameId) {
        return squadService.getAllGameSquads(gameId);
    }

    @PostMapping()
    public ResponseEntity<Squad> addGameSquad(@PathVariable String gameId, @RequestBody Squad squad) {
        return squadService.addGameSquad(gameId, squad);
    }

    @GetMapping("/{squadId}")
    public ResponseEntity<Squad> getGameSquad(@PathVariable String gameId, @PathVariable String squadId) {
        return squadService.getGameSquad(gameId, squadId);
    }

    @PutMapping("/{squadId}")
    public ResponseEntity<Squad> updateGameSquad(@PathVariable String gameId, @PathVariable String squadId, @RequestBody Squad squad) {
        return squadService.updateSquad(gameId, squadId, squad);
    }

    @DeleteMapping("/{squadId}")
    public ResponseEntity<Squad> deleteGameSquad(@PathVariable String gameId, @PathVariable String squadId) {
        return squadService.deleteSquad(gameId, squadId);
    }
}
