package com.hvz.controllers;

import com.hvz.models.User;
import com.hvz.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService = new UserService();

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(){
        return userService.getAllUsers();
    }

    // Get specific user by id
    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id){
        return userService.getUserById(id);
    }

    // Get specific user by email
    @GetMapping("/email/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable String email){
        return userService.getUserByEmail(email);
    }

    // Insert a new user
    @PostMapping("/register")
    public ResponseEntity<User> addUser(@RequestBody User user){
        return userService.addUser(user);
    }

    // Update user
    // Only Admin
    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable String id,@RequestBody User user){
        return userService.updateUser(id, user);
    }
    // delete user
    // Only Admin
    @DeleteMapping("/{email}")
    public ResponseEntity<User> deleteUserByEmail(@PathVariable String email){
        return userService.deleteUser(email);
    }


}
