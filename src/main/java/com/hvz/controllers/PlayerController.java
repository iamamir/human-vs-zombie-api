package com.hvz.controllers;

import com.hvz.models.Player;
import com.hvz.services.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/game/{game_id}/player")
public class PlayerController {

    PlayerService playerService = new PlayerService();

    @GetMapping()
    public ResponseEntity<List<Player>> getAllPlayers(@PathVariable String game_id){
        return playerService.getAllPlayers(game_id);
    }

    @GetMapping("/{player_id}")
    public ResponseEntity<Player> getPlayer(@PathVariable String game_id, @PathVariable String player_id){
        return playerService.getPlayer(game_id,player_id);
    }

    @GetMapping("/current")
    public ResponseEntity<Player> getCurrentPlayer(@PathVariable String game_id){
        return playerService.getLoggedPlayer(game_id);
    }

    @PostMapping()
    public ResponseEntity<Player> addPlayer(@PathVariable String game_id, @RequestBody Player player){
        return playerService.addPlayer(game_id,player);
    }

    @PutMapping("/{player_id}")
    public ResponseEntity<Player> updatePlayer(@PathVariable String game_id,  @PathVariable String player_id, @RequestBody Player player){

        return playerService.updatePlayer(game_id,player_id,player);
    }

    @DeleteMapping("/{player_id}")
    public ResponseEntity<Player> deletePlayer(@PathVariable String player_id, @PathVariable String game_id){
        return playerService.deletePlayer(player_id, game_id);
    }

}
