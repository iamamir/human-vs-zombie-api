package com.hvz.controllers;

import com.hvz.models.Message;
import com.hvz.services.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@RequestMapping("api/v1/game/{gameId}")
public class MessagesController {
    /*
    private final MessageService messageService = new MessageService();
    //types
    //       -> 1 -> global chat for all
    //       -> 2 -> global chat for zombies only
    //       -> 3 -> global chat for human only

    @GetMapping("/message/{type}")
    public ResponseEntity<ArrayList<Message>> getGameGlobalMessages(@PathVariable String gameId, @PathVariable String type) {
        return messageService.getGameMessages(gameId, type);
    }

    @GetMapping("/squad/{squadId}/message")
    public ResponseEntity<ArrayList<Message>> getSquadMessages(@PathVariable String gameId, @PathVariable String squadId) {
        return messageService.getSquadMessages(gameId, squadId);
    }

    @PostMapping("/message")
    public ResponseEntity<Message> addGameGlobalMessage(@PathVariable String gameId, @RequestBody Message message) {
        return messageService.addGlobalGameMessage(gameId, message);
    }

    @PostMapping("/squad/{squadId}/message")
    public ResponseEntity<Message> addSquadMessage(@PathVariable String gameId,@PathVariable String squadId, @RequestBody Message message) {
        return messageService.addSquadMessage(gameId,squadId, message);
    }

     */
}
