package com.hvz.controllers;

import com.hvz.models.Mission;
import com.hvz.services.MissionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/game/{gameId}/mission")
public class MissionController {

    private final MissionService missionService = new MissionService();


    @GetMapping()
    public ResponseEntity<List<Mission>> getAllGameMissions(@PathVariable String gameId) {
        return missionService.getAllGameMissions(gameId);
    }

    @PostMapping()
    public ResponseEntity<Mission> addGameMission(@PathVariable String gameId, @RequestBody Mission mission) {
        return missionService.addGameMission(gameId,mission);
    }

    @GetMapping("/{missionId}")
    public ResponseEntity<Mission> getGameMission(@PathVariable String gameId, @PathVariable String missionId) {
        return missionService.getGameMission(gameId,missionId);
    }

    @PutMapping("/{missionId}")
    public ResponseEntity<Mission> updateMission(@PathVariable String gameId, @PathVariable String missionId,@RequestBody Mission mission) {
        return missionService.updateGameMission(gameId,missionId,mission);
    }

    @DeleteMapping("/{missionId}")
    public ResponseEntity<Mission> deleteGameMission(@PathVariable String gameId, @PathVariable String missionId) {
        return missionService.deleteMission(gameId,missionId);
    }
}
