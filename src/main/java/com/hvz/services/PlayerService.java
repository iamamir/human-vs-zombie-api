package com.hvz.services;

import com.hvz.database.PlayerDB;
import com.hvz.helpers.BiteCodeHelper;
import com.hvz.models.Game;
import com.hvz.models.Player;
import com.hvz.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class PlayerService {

    // Databases
    private final PlayerDB playerDB = new PlayerDB();

    //Services
    private final BiteCodeHelper biteCodeHelper = new BiteCodeHelper();
    private final GameService gameService = new GameService();
    private final UserService userService = new UserService();

    // Objects
    Player dbPlayer = null;

    public ResponseEntity<List<Player>> getAllPlayers(String game_id) {
        boolean gameExist = gameService.gameExist(game_id);
        if (!gameExist)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        List<Player> players = new ArrayList<>();

        if (isAdmin())
            players = playerDB.getAllPlayersAsAdmin(game_id);
        else
            players = playerDB.getAllPlayers(game_id);

        if (players.size() > 0)
            return new ResponseEntity<>(players, HttpStatus.OK);
        else
            return new ResponseEntity<>(players, HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Player> getPlayer(String game_id, String player_id) {
        boolean gameExist = gameExist(game_id);
        if (!gameExist)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!playerExist(player_id, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (isAdmin())
            dbPlayer = playerDB.getPlayerAsAdmin(game_id, player_id);
        else
            dbPlayer = playerDB.getPlayer(game_id, player_id);

        return new ResponseEntity<>(dbPlayer, HttpStatus.OK);
    }

    public ResponseEntity<Player> getLoggedPlayer(String game_id) {

        String email = userService.getCurrentUser();
        return getPlayerByEmail(email, game_id);
    }

    public ResponseEntity<Player> getPlayerByEmail(String email, String game_id) {

        ResponseEntity<User> userResponseEntity = userService.getUserByEmail(email);
        if (userResponseEntity.getStatusCode() != HttpStatus.OK) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return getPlayerByUserId(userResponseEntity.getBody().getId(), game_id);
    }

    public ResponseEntity<Player> getPlayerByUserId(String user_id, String game_id) {
        if (!playerExistByUserId(user_id, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(playerDB.getPlayerByUserId(user_id, game_id), HttpStatus.OK);
    }

    //only logged users can add him/her-self as a player
    public ResponseEntity<Player> addPlayer(String game_id, Player player) {

        String currentEmail = userService.getCurrentUser();
        ResponseEntity<User> userResponseEntity = userService.getUserByEmail(currentEmail);
        if (userResponseEntity.getStatusCode() != HttpStatus.OK)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);


        User user = userResponseEntity.getBody();
        player.setUserId(user.getId());


        ResponseEntity<Game> gameResponseEntity = getGame(game_id);

        if (gameResponseEntity.getStatusCode() != HttpStatus.OK)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        player.setGameId(game_id);


        if (!gameResponseEntity.getBody().getGame_state().equalsIgnoreCase("registration"))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);


        boolean playerExist = playerExistByUserId(player.getUserId(), game_id);
        if (playerExist)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);


        boolean userNameExist = userNameExist(game_id, player.getUserName());
        if (userNameExist)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        // Check if the bite code exist
        String bite_code;
        do {
            bite_code = biteCodeHelper.generateBiteCode();
        }
        while (playerDB.biteCodeExist(bite_code, game_id));

        if (playerDB.addPlayer(game_id, player, bite_code))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Player> updatePlayer(String game_id, String player_id, Player player) {
        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!playerExist(player_id, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        dbPlayer = playerDB.getPlayerAsAdmin(game_id, player_id);

        if (player.getUserName() != null)
            dbPlayer.setUserName(player.getUserName());

        if (player.getBiteCode() != null) {
            if (!player.getBiteCode().equalsIgnoreCase(dbPlayer.getBiteCode())) {
                if (biteCodeExist(player.getBiteCode(), game_id))
                    return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
                else
                    dbPlayer.setBiteCode(player.getBiteCode());
            } else
                dbPlayer.setBiteCode(player.getBiteCode());
        }

        if (player.getIs_human() != null)
            dbPlayer.setIs_human(player.getIs_human());

        if (player.getIs_patient_zero() != null)
            dbPlayer.setIs_patient_zero(player.getIs_patient_zero());


        if (playerDB.updatePlayer(dbPlayer))
            return new ResponseEntity<>(null, HttpStatus.OK);


        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Player> deletePlayer(String player_id, String game_id) {

        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);


        if (!playerExist(player_id, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (playerDB.deletePlayer(game_id, player_id))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Player> getPlayerInternal(String game_id, String player_id) {

        if (!gameExist(game_id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        if (!playerExist(player_id, game_id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(playerDB.getPlayerAsAdmin(game_id, player_id), HttpStatus.OK);
    }

    public ResponseEntity<Player> getPlayerByBiteCode(String bite_code, String game_id) {
        if (!biteCodeExist(bite_code, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(playerDB.getPlayerByBiteCode(bite_code, game_id), HttpStatus.OK);
    }

    public ResponseEntity<Player> killPlayer(String player_id) {
        if (playerDB.killPlayer(player_id))
            return new ResponseEntity<>(null, HttpStatus.OK);
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Player> revivePlayer(String player_id) {
        if (playerDB.revivePlayer(player_id))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Game> getGame(String game_id) {
        return gameService.getGame(game_id);
    }

    public boolean isAdmin() {
        return userService.isAdmin();
    }

    public boolean gameExist(String game_id) {
        return gameService.gameExist(game_id);
    }

    public boolean playerExist(String player_id, String game_id) {
        return playerDB.playerExist(player_id, game_id);
    }


    public boolean playerExistByUserId(String user_id, String game_id) {
        return playerDB.playerExistByUserId(user_id, game_id);
    }

    public boolean biteCodeExist(String bite_code, String game_id) {
        return playerDB.biteCodeExist(bite_code, game_id);
    }

    public boolean userNameExist(String game_id, String username) {
        return playerDB.userNameExistInGame(game_id, username);
    }


}
