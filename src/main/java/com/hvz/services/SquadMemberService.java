package com.hvz.services;

import com.hvz.database.SquadDB;
import com.hvz.database.SquadMemberDB;
import com.hvz.models.Player;
import com.hvz.models.SquadMember;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;

public class SquadMemberService {

    // Databases
    private final SquadMemberDB squadMemberDB = new SquadMemberDB();
    private final SquadDB squadDB = new SquadDB();

    // Services
    private final GameService gameService = new GameService();
    private final PlayerService playerService = new PlayerService();
    private final UserService userService = new UserService();

    public ResponseEntity<ArrayList<SquadMember>> getAllSquadMembers(String game_id, String squad_id) {
        boolean gameExist = gameExist(game_id);
        if(!gameExist){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }

        return squadMemberDB.getAllSquadMembers(game_id, squad_id);
    }

    public ResponseEntity<SquadMember> addSquadMember(String game_id, String squad_id) {

        boolean gameExist = gameExist(game_id);
        if(!gameExist){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }

        String currentUser = userService.getCurrentUser();

        ResponseEntity<Player> playerResponseEntity = playerService.getPlayerByEmail(currentUser,game_id);

        if(playerResponseEntity.getStatusCode() != HttpStatus.OK){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return squadMemberDB.addSquadMember(game_id, squad_id, playerResponseEntity.getBody().getId());
    }

    public ResponseEntity<SquadMember> addSquadLeader(String game_id, String squad_id, String player_id){
        boolean gameExist = gameExist(game_id);
        if(!gameExist){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }


        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }

        boolean isMember = isMember(game_id, squad_id, player_id);
        if(!isMember){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return squadMemberDB.setSquadLeader(game_id, squad_id, player_id);
    }


    public boolean gameExist(String game_id){
        return gameService.gameExist(game_id);
    }

    public boolean squadExist(String game_id, String squad_id){
        return squadDB.squadExist(game_id, squad_id);
    }

    public boolean isMember(String game_id, String squad_id, String player_id){
        return squadMemberDB.isMember(game_id, squad_id, player_id);
    }

}
