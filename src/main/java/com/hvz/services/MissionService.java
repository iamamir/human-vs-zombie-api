package com.hvz.services;

import com.hvz.database.MissionDB;
import com.hvz.models.Mission;
import com.hvz.models.Player;
import com.hvz.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class MissionService {

    private final MissionDB missionDB = new MissionDB();

    // Services
    private final GameService gameService = new GameService();
    private final UserService userService = new UserService();
    private final PlayerService playerService = new PlayerService();


    public ResponseEntity<List<Mission>> getAllGameMissions(String gameId) {
        List<Mission> missions = new ArrayList<>();

        if (gameExist(gameId)) {

            if (playerService.isAdmin())
                missions = missionDB.getGameMissions(gameId);

            else {
                String email = getCurrentUser();
                User user = getUserByEmail(email);
                Player player = getPlayerById(user.getId(), gameId);

                List<Mission> globalMissions = missionDB.getGameMissionsFactionSpecific(gameId, "1", "1");
                missions.addAll(globalMissions);

                if (player.getIs_human().equalsIgnoreCase("1")) {
                    List<Mission> humanMissions = missionDB.getGameMissionsFactionSpecific(gameId, "1", "0");
                    missions.addAll(humanMissions);
                }

                if (player.getIs_human().equalsIgnoreCase("0")) {
                    List<Mission> zombieMissions = missionDB.getGameMissionsFactionSpecific(gameId, "0", "1");
                    missions.addAll(zombieMissions);
                }
            }

            if (missions.size() > 0)
                return new ResponseEntity<>(missions, HttpStatus.OK);
            else
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<Mission> addGameMission(String gameId, Mission mission) {

        if (!playerService.isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(gameId))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        mission.setGame_id(gameId);

        if (mission.getMission_name() == null
                || mission.getIs_human_visible() == null
                || mission.getIs_zombie_visible() == null
                || mission.getDescription() == null
                || mission.getStart_time() == null
                || mission.getEnd_time() == null
                || mission.getLatitude() == null
                || mission.getLongitude() == null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);


        if (missionDB.addMission(mission))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Mission> getGameMission(String game_id, String missionId) {

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!missionExist(game_id, missionId))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        Mission mission = missionDB.getGameMission(game_id, missionId);

        if (playerService.isAdmin())
            return new ResponseEntity<>(mission, HttpStatus.OK);

        String currentEmail = getCurrentUser();
        User currentUser = getUserByEmail(currentEmail);
        Player currentPlayer = getPlayerById(currentUser.getId(), game_id);

        if (currentPlayer.getIs_human().equalsIgnoreCase(mission.getIs_human_visible()))
            return new ResponseEntity<>(mission, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }


    public ResponseEntity<Mission> updateGameMission(String gameId, String missionId, Mission updatedMission) {

        if (!playerService.isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(gameId))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!missionExist(gameId, missionId)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        updatedMission.setGame_id(gameId);
        updatedMission.setMission_id(missionId);


        Mission originalMission = missionDB.getGameMission(gameId, missionId);

        if (updatedMission.getMission_name() == null)
            updatedMission.setMission_name(originalMission.getMission_name());

        if (updatedMission.getIs_human_visible() == null)
            updatedMission.setIs_human_visible(originalMission.getIs_human_visible());

        if (updatedMission.getIs_zombie_visible() == null)
            updatedMission.setIs_zombie_visible(originalMission.getIs_zombie_visible());

        if (updatedMission.getDescription() == null)
            updatedMission.setDescription(originalMission.getDescription());

        if (updatedMission.getStart_time() == null)
            updatedMission.setStart_time(originalMission.getStart_time());

        if (updatedMission.getEnd_time() == null)
            updatedMission.setEnd_time(originalMission.getEnd_time());


        if (missionDB.updateMission(gameId, missionId, updatedMission))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Mission> deleteMission(String game_id, String mission_id) {

        if (!playerService.isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);


        if (!missionExist(game_id, mission_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (missionDB.deleteMission(game_id, mission_id))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }


    public boolean missionExist(String game_id, String mission_id) {
        return missionDB.isMissionExist(game_id, mission_id);
    }

    public boolean gameExist(String game_id) {
        return gameService.gameExist(game_id);
    }

    public String getCurrentUser() {
        return userService.getCurrentUser();
    }

    public User getUserByEmail(String email) {
        return userService.getUserByEmail(email).getBody();
    }

    public Player getPlayerById(String user_id, String game_id) {
        return playerService.getPlayerByUserId(user_id, game_id).getBody();
    }


}
