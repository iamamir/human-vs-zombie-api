package com.hvz.services;

import com.hvz.database.MessagesDB;
import com.hvz.models.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;

public class MessageService {
   /*
    private final MessagesDB messagesDB = new MessagesDB();
    private final GameService gameService = new GameService();
    private final SquadService squadService = new SquadService();
    private final PlayerService playerService = new PlayerService();

    //types
    //       -> 1 -> global chat for all
    //       -> 2 -> global chat for zombies only
    //       -> 3 -> global chat for human only

    public ResponseEntity<ArrayList<Message>> getGameMessages(String game_id, String type) {

        boolean gameExist = gameExist(game_id);
        if (!gameExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        String is_human_global = "0";
        String is_zombie_global= "0";
        if(type.equals("1")){
            is_zombie_global = "1";
            is_human_global = "1";
        }else if(type.equals("2")){
            is_zombie_global = "1";
        }else{
            is_human_global = "1";
        }

        return messagesDB.getGameMessages(game_id,null,is_human_global,is_zombie_global);
    }

    public ResponseEntity<ArrayList<Message>> getSquadMessages(String game_id, String squad_id) {

        boolean gameExist = gameExist(game_id);
        if (!gameExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return messagesDB.getSquadMessages(game_id,squad_id);
    }

    public ResponseEntity<Message> addGlobalGameMessage(String game_id, Message message) {

        if (messageAdderChecker(game_id, message)) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return messagesDB.addMessage(message);
    }

    public ResponseEntity<Message> addSquadMessage(String game_id, String squad_id, Message message) {

        if (messageAdderChecker(game_id, message)) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        if (!squad_id.equalsIgnoreCase(message.getSquadId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return messagesDB.addMessage(message);
    }

    private boolean messageAdderChecker(String game_id, Message message) {

        boolean gameExist = gameExist(game_id);
        if (!gameExist) {
            return true;
        }

        if (!game_id.equalsIgnoreCase(message.getGameId())) {
            return true;
        }

        if (message.getPlayerId() == null) {
            return true;
        }
        if (message.getIsZombieGlobal() == null || message.getIsHumanGlobal() == null) {
            return true;
        }

        boolean playerExist = playerExist(message.getPlayerId(), game_id);
        if (!playerExist) {
            return true;
        }

        if (message.getMessageBody() == null) {
            return true;
        }

        return false;
    }

    public boolean gameExist(String game_id){
        return gameService.gameExist(game_id);
    }
    public boolean squadExist(String game_id, String squad_id){
        return squadService.squadExist(game_id,squad_id);
    }
    public boolean playerExist(String player_id, String game_id){
        return playerService.playerExist(player_id, game_id);
    }
*/
}
