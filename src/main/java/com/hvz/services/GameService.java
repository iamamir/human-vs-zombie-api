package com.hvz.services;

import com.hvz.database.GameDB;
import com.hvz.models.Game;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;


import java.util.List;

public class GameService {

    // Services
    UserService userService = new UserService();

    // Databases
    private final GameDB gameDB = new GameDB();

    public ResponseEntity<List<Game>> getAllGames() {
        List<Game> games = gameDB.getAllGames();
        if (games.size() > 0)
            return new ResponseEntity<>(games, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Game> getGame(String gameId) {
        if (gameExist(gameId))
            return new ResponseEntity<>(gameDB.getGame(gameId), HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Game> addGame(Game game) {
        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (game.getGame_name() == null ||
                game.getNw_latitude() == null ||
                game.getNw_longitude() == null ||
                game.getSe_latitude() == null ||
                game.getSe_longitude() == null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);


        if (gameDB.addGame(game))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Game> updateGame(String gameId, Game updatedGame) {

        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(gameId))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        updatedGame.setId(gameId);

        Game originalGame = gameDB.getGame(gameId);

        if (updatedGame.getGame_name() == null)
            updatedGame.setGame_name(originalGame.getGame_name());


        if (updatedGame.getGame_state() == null)
            updatedGame.setGame_state(originalGame.getGame_state());

        if (updatedGame.getNw_latitude() == null)
            updatedGame.setNw_latitude(originalGame.getNw_latitude());

        if (updatedGame.getNw_longitude() == null)
            updatedGame.setNw_longitude(originalGame.getNw_longitude());

        if (updatedGame.getSe_latitude() == null)
            updatedGame.setSe_latitude(originalGame.getSe_latitude());

        if (updatedGame.getSe_longitude() == null)
            updatedGame.setSe_longitude(originalGame.getSe_longitude());

        if (gameDB.updateGame(updatedGame))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Game> deleteGame(String game_id) {

        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (gameDB.deleteGame(game_id))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public boolean isAdmin() {
        return userService.isAdmin();
    }

    public boolean gameExist(String gameId) {
        return gameDB.gameExist(gameId);
    }

}
