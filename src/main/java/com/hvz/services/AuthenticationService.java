package com.hvz.services;

import com.hvz.database.UserDB;
import com.hvz.helpers.PasswordHelper;
import com.hvz.logger.Logger;
import com.hvz.models.AuthenticationResponse;
import com.hvz.models.User;
import com.hvz.security.JwtUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AuthenticationService {

    private final PasswordHelper passwordHelper = new PasswordHelper();
    private final UserService userService = new UserService();
    private final JwtUtil jwtUtil = new JwtUtil();

    Logger logger = new Logger();

    // Login user
    public ResponseEntity<AuthenticationResponse> requestLogin(User user) {

        // Check if a user exist with email
        boolean userExist = userService.userExist(user.getEmail());
        if (!userExist) {
            logger.log("User : " + user.getEmail() + " was not found in the database");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        User dbUser = new UserDB().getUserByEmail(user.getEmail());

        // Check if password matches
        boolean isPasswordCorrect = isPasswordCorrect(dbUser, user);
        if (!isPasswordCorrect) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        final String jwt = jwtUtil.generateToken(user);

        String role;

        if (dbUser.getIsAdmin().equalsIgnoreCase("1"))
            role = "admin";
        else
            role = "user";

        AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwt,role);

        return new ResponseEntity<>(authenticationResponse, HttpStatus.OK);
    }

    public boolean isPasswordCorrect(User dbUser, User requestUser) {
        return passwordHelper.isExpectedPassword(requestUser.getPassword(), dbUser.getSalt(), dbUser.getPassword());
    }


}
