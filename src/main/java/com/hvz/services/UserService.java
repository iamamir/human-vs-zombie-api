package com.hvz.services;

import com.hvz.database.UserDB;
import com.hvz.models.User;
import com.hvz.models.CurrentUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class UserService {

    private final UserDB userDB = new UserDB();
    private final CurrentUser currentUser = CurrentUser.getInstance();

    // Get a list of all users
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userDB.getAllUsers();
        if (users.size() > 0)
            return new ResponseEntity<>(users, HttpStatus.OK);
        
        return new ResponseEntity<>(users, HttpStatus.NO_CONTENT);
    }

    // Get a user by id
    public ResponseEntity<User> getUserById(String user_id) {
        if (userExist(user_id))
            return new ResponseEntity<>(userDB.getUserById(user_id), HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    // Get a user by email
    public ResponseEntity<User> getUserByEmail(String email) {
        if (userExist(email))
            return new ResponseEntity<>(userDB.getUserByEmail(email), HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    // Add a user
    public ResponseEntity<User> addUser(User user) {
        if (user.getEmail() == null || user.getPassword() == null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        if (userExist(user.getEmail()))
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);

        if (userDB.addUser(user.getEmail(), user.getPassword()))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<User> updateUser(String user_id, User user) {
        if (!isAdmin())
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        if (!userExist(user_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (user.getId() != null)
            if (!user.getId().equalsIgnoreCase(user_id))
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        if (user.getEmail() == null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        user.setId(user_id);

        if (userDB.updateUser(user))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // Delete user
    public ResponseEntity<User> deleteUser(String email) {
        if (!isAdmin()) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

        boolean userExist = userExist(email);
        if (!userExist) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        if (userDB.deleteUser(email))
            return new ResponseEntity<>(null, HttpStatus.OK);
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public String getCurrentUser() {
        return currentUser.getEmail().trim();
    }

    public boolean isAdmin() {
        return userDB.checkIsAdmin(getCurrentUser());
    }

    public boolean userExist(String emailOrId) {
        if (emailOrId.contains("@")) {
            return userDB.checkIfUserExistEmail(emailOrId);
        }
        return userDB.checkIfUserExistById(emailOrId);
    }
}
