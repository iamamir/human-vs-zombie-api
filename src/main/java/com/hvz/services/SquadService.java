package com.hvz.services;

import com.hvz.database.SquadDB;
import com.hvz.models.Player;
import com.hvz.models.Squad;
import com.hvz.models.SquadMember;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;

public class SquadService {
    // Databases
    private final SquadDB squadDB = new SquadDB();

    // Services
    private final GameService gameService = new GameService();
    private final UserService userService = new UserService();
    private final PlayerService playerService = new PlayerService();
    private final SquadMemberService squadMemberService = new SquadMemberService();

    public ResponseEntity<ArrayList<Squad>> getAllGameSquads(String game_id) {

        boolean gameExist = gameExist(game_id);
        if (!gameExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return squadDB.getAllGameSquad(game_id);
    }

    public ResponseEntity<Squad> getGameSquad(String game_id, String squad_id) {
        boolean gameExist = gameService.gameExist(game_id);
        if (!gameExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return squadDB.getGameSquad(game_id, squad_id);

    }

    public ResponseEntity<Squad> addGameSquad(String game_id, Squad squad) {

        boolean ifGameExist = gameExist(game_id);
        if (!ifGameExist){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        String email = getCurrentUser();

        ResponseEntity<Player> playerResponseEntity = playerService.getPlayerByEmail(email,game_id);

        if(playerResponseEntity.getStatusCode() != HttpStatus.OK){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Player player = playerResponseEntity.getBody();

        squad.setIsHuman(player.getIs_human());

        if (squad.getSquadName() == null || squad.getGameId() == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        ResponseEntity<Squad> squadResponseEntity = squadDB.addSquad(squad);

        if(squadResponseEntity.getStatusCode() != HttpStatus.OK){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Squad dbSquad = squadResponseEntity.getBody();

        ResponseEntity<SquadMember> squadMemberResponseEntity = squadMemberService.addSquadLeader(game_id,dbSquad.getSquadId(),player.getId());

        if(squadMemberResponseEntity.getStatusCode() != HttpStatus.OK){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public ResponseEntity<Squad> updateSquad(String game_id, String squad_id, Squad squad) {

        boolean isAdmin = isAdmin();

        if(!isAdmin){
            return new ResponseEntity<>(null,HttpStatus.FORBIDDEN);
        }

        ResponseEntity<Squad> squadResponseEntity = getGameSquad(game_id, squad_id);
        if(squadResponseEntity.getStatusCode() != HttpStatus.OK){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        Squad dbSquad = squadResponseEntity.getBody();

        if (squad.getSquadName() != null){
            dbSquad.setSquadName(squad.getSquadName());
        }

        return squadDB.updateSquad(dbSquad.getGameId(),dbSquad.getSquadId(), dbSquad.getSquadName());
    }

    public ResponseEntity<Squad> deleteSquad(String game_id, String squad_id) {

        boolean isAdmin = isAdmin();
        if(!isAdmin){
            return new ResponseEntity<>(null,HttpStatus.FORBIDDEN);
        }

        boolean gameExist = gameExist(game_id);
        if (!gameExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

        }
        boolean squadExist = squadExist(game_id, squad_id);
        if (!squadExist) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return squadDB.deleteSquad(game_id,squad_id);
    }

    public boolean gameExist(String game_id){
        return gameService.gameExist(game_id);
    }

    public boolean squadExist(String gameId, String squadId) {
        return squadDB.squadExist(gameId, squadId);
    }

    public boolean isAdmin(){
        return userService.isAdmin();
    }

    public String getCurrentUser(){
        return userService.getCurrentUser();
    }



}
