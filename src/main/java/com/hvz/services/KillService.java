package com.hvz.services;

import com.hvz.database.KillDB;
import com.hvz.database.PlayerDB;
import com.hvz.models.Kill;
import com.hvz.models.Player;
import com.hvz.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class KillService {

    // Database
    KillDB killDB = new KillDB();

    // Services
    GameService gameService = new GameService();
    PlayerService playerService = new PlayerService();
    UserService userService = new UserService();

    // Objects
    Kill dbKill = null;
    Player dbPlayer = null;


    // Returns a list containing all kills from the game provided
    public ResponseEntity<List<Kill>> getAllKills(String game_id) {
        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        List<Kill> kills = killDB.getAllKills(game_id);
        if (kills.size() > 0)
            return new ResponseEntity<>(kills, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    // Returns a specific kill object
    public ResponseEntity<Kill> getKill(String game_id, String kill_id) {

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!killExist(kill_id, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(killDB.getKill(game_id, kill_id), HttpStatus.OK);
    }

    public ResponseEntity<Kill> addKill(String bite_code, String game_id, Kill kill) {

        if (!gameExist(game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        kill.setGameId(game_id);

        if (!biteCodeExist(bite_code, game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        // Get the victim player by bite code - If the bite code exist, a player with that code must also exist
        dbPlayer = playerService.getPlayerByBiteCode(bite_code, game_id).getBody();

        // Check if the victim is already dead
        if (dbPlayer.getIs_human().equalsIgnoreCase("0"))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        kill.setKillerId(playerService.getPlayerByEmail(userService.getCurrentUser(), game_id).getBody().getId());

        // Check if killer exist in the game
        if (!playerExist(kill.getKillerId(), kill.getGameId()))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        // Set the victim in the kill object
        kill.setVictimId(dbPlayer.getId());

        // Update the status of the victim
        if (!killPlayer(dbPlayer.getId()))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        if (killDB.addKill(kill))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // Updates a kill. Only Admin or the killer may update a kill
    public ResponseEntity<Kill> updateKill(String game_id, String kill_id, Kill kill) {

        boolean isAdmin = isAdmin();
        boolean isKiller = isKiller(game_id, kill.getKillerId());

        if (!isAdmin && !isKiller)
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

        ResponseEntity<Kill> killResponseEntity = getKill(game_id, kill_id);
        if (killResponseEntity.getStatusCode() != HttpStatus.OK)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        dbKill = killResponseEntity.getBody();

        if (kill.getTimeOfDeath() != null)
            dbKill.setTimeOfDeath(kill.getTimeOfDeath());

        if (kill.getStory() != null)
            dbKill.setStory(kill.getStory());

        if (kill.getLatitude() != null)
            dbKill.setLatitude(kill.getLatitude());

        if (kill.getLongitude() != null)
            dbKill.setLongitude(kill.getLongitude());

        if (killDB.updateKill(dbKill))
            return new ResponseEntity<>(null, HttpStatus.OK);

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Kill> deleteKill(String game_id, String kill_id) {

        boolean isAdmin = isAdmin();
        if (!isAdmin) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

        ResponseEntity<Kill> killResponseEntity = getKill(game_id, kill_id);
        if (killResponseEntity.getStatusCode() != HttpStatus.OK) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        dbKill = killResponseEntity.getBody();

        if (!playerExist(dbKill.getVictimId(), game_id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (!revivePlayer(dbKill.getVictimId()))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        if (killDB.deleteKill(game_id, kill_id))
            return new ResponseEntity<>(null, HttpStatus.OK);
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public boolean isAdmin() {
        return userService.isAdmin();
    }

    public boolean killExist(String kill_id, String game_id) {
        return killDB.checkIfKillExist(kill_id, game_id);
    }

    public boolean gameExist(String game_id) {
        return gameService.gameExist(game_id);
    }

    public boolean playerExist(String player_id, String game_id) {
        return playerService.playerExist(player_id, game_id);
    }

    public boolean biteCodeExist(String bite_code, String game_id) {
        return playerService.biteCodeExist(bite_code, game_id);
    }

    public boolean isKiller(String game_id, String killerId) {

        // Get the player from the database
        Player player = playerService.getPlayerInternal(game_id, killerId).getBody();

        // Get the user belonging to the player
        User dbUser = userService.getUserById(player.getUserId()).getBody();
        String dbEmail = dbUser.getEmail();

        // Get the email of the current user making the request
        String tokenEmail = userService.getCurrentUser();

        return tokenEmail.equals(dbEmail);
    }

    public boolean killPlayer(String player_id) {
        return playerService.killPlayer(player_id).getStatusCode().equals(HttpStatus.OK);
    }

    public boolean revivePlayer(String player_id) {
        return playerService.revivePlayer(player_id).getStatusCode().equals(HttpStatus.OK);
    }

}
