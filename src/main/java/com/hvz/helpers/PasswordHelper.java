package com.hvz.helpers;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

public class PasswordHelper {

    private static final Random random = new SecureRandom();
    private static final int ITERATIONS = 10000;
    private static final int KEY_LENGTH = 256;

    public PasswordHelper() { }

    public String generateSalt(){

        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return byteToString(salt);
    }

    public String hash(String password, String salt) {

        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), stringToByte(salt), ITERATIONS, KEY_LENGTH);
        Arrays.fill(password.toCharArray(), Character.MIN_VALUE);
        SecretKeyFactory skf;
        byte[] hashedPassword;
        try {
            skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hashedPassword = skf.generateSecret(spec).getEncoded();
            spec.clearPassword();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return byteToString(hashedPassword);
    }

    public boolean isExpectedPassword(String password, String salt, String expectedHash) {

        String pwdHash = hash(password, salt);

        Arrays.fill(password.toCharArray(), Character.MIN_VALUE);

        if (pwdHash.length() != expectedHash.length()){
            return false;
        }

        for (int i = 0; i < pwdHash.length(); i++) {
            if (pwdHash.charAt(i) != (expectedHash.charAt(i))){
                return false;
            }
        }

        return true;
    }

    private String byteToString(byte[] bytes){
        return Base64.getEncoder().encodeToString(bytes);
    }

    private byte[] stringToByte(String string){
        return Base64.getDecoder().decode(string);
    }



}
