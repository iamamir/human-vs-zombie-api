package com.hvz.helpers;

import com.hvz.logger.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;

import java.util.Properties;

public class SendEmailHelper {

    private final Logger logger = new Logger();

    public boolean sendEmail(String email, String token) {
        boolean emailSent = false;
        Properties prop = new Properties();
        try (FileReader reader = new FileReader("src/main/resources/sendemail.properties")) {
            prop.load(reader);
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true");
            /*
             * prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
             * prop.put("mail.smtp.host", "smtp.gmail.com");
             * prop.put("mail.smtp.port", "587");
             */
            prop.put("mail.smtp.host", "smtp.mailtrap.io");

            prop.put("mail.smtp.port", "2525");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(prop.getProperty("sendEmailUsername"), prop.getProperty("sendEmailPassword"));
                    }
                });
        try {
            Message message = new MimeMessage(session);
            //  message.setFrom(new InternetAddress(prop.getProperty("sendEmailUsername")));
            message.setFrom(new InternetAddress("Sender Name" + "<" + "no-reply@domain.com" + ">"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("AUTH TOKEN");
            message.setText("Hello!\nHere's your authentication token: " + token);
            Transport.send(message);
            logger.log("An email was sent successfully");
            emailSent = true;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return emailSent;
    }
}
