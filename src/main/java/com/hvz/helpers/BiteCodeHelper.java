package com.hvz.helpers;

import java.util.Random;

public class BiteCodeHelper {
    private final Random rd = new Random();
    public String generateBiteCode(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 6; i++){
            sb.append(rd.nextInt(10));

        }
        return sb.toString();
    }
}
