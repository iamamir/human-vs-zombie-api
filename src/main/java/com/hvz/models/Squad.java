package com.hvz.models;

public class Squad {
    private String squad_id;
    private String squad_name;
    private String is_human;
    private String game_id;

    public Squad(){

    }

    public Squad(String squad_id, String squad_name, String is_human, String game_id) {
        this.squad_id = squad_id;
        this.squad_name = squad_name;
        this.is_human = is_human;
        this.game_id = game_id;
    }

    public String getSquadId() {
        return squad_id;
    }

    public void setSquadId(String squad_id) {
        this.squad_id = squad_id;
    }

    public String getSquadName() {
        return squad_name;
    }

    public void setSquadName(String squad_name) {
        this.squad_name = squad_name;
    }

    public String getIsHuman() {
        return is_human;
    }

    public void setIsHuman(String is_human) {
        this.is_human = is_human;
    }

    public String getGameId() {
        return game_id;
    }

    public void setGameId(String game_id) {
        this.game_id = game_id;
    }
}
