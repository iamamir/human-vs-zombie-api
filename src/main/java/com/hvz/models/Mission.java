package com.hvz.models;

public class Mission {

    private String mission_id;
    private String mission_name;
    private String is_human_visible;
    private String is_zombie_visible;
    private String description;
    private String start_time;
    private String end_time;
    private String game_id;
    private String latitude;
    private String longitude;

    public Mission(){

    }

    public Mission(String mission_id, String mission_name, String is_human_visible, String is_zombie_visible, String description, String start_time, String end_time, String game_id, String latitude, String longitude) {
        this.mission_id = mission_id;
        this.mission_name = mission_name;
        this.is_human_visible = is_human_visible;
        this.is_zombie_visible = is_zombie_visible;
        this.description = description;
        this.start_time = start_time;
        this.end_time = end_time;
        this.game_id = game_id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getMission_id() {
        return mission_id;
    }

    public void setMission_id(String mission_id) {
        this.mission_id = mission_id;
    }

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String getIs_human_visible() {
        return is_human_visible;
    }

    public void setIs_human_visible(String is_human_visible) {
        this.is_human_visible = is_human_visible;
    }

    public String getIs_zombie_visible() {
        return is_zombie_visible;
    }

    public void setIs_zombie_visible(String is_zombie_visible) {
        this.is_zombie_visible = is_zombie_visible;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
