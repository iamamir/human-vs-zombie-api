package com.hvz.models;

public class User{

    private String id;
    private String email;
    private String password;
    private String salt;
    private String isAdmin;

    public User(){

    }

    public User(String id, String email, String password, String salt, String isAdmin) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.isAdmin = isAdmin;
    }
    public User(String id, String email, String password, String salt) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.salt = salt;
    }

    public User(String email, String hashedPassword, String salt) {
        this.email = email;
        this.password = hashedPassword;
        this.salt = salt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

}
