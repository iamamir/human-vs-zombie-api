package com.hvz.models;

public class Player {

    private String id;
    private String user_name;
    private String bite_code;
    private String is_human;
    private String is_patient_zero;
    private String user_id;
    private String game_id;

    public Player() {

    }

    public Player(String id, String user_name, String game_id, String user_id) {
        this.id = id;
        this.user_name = user_name;
        this.game_id = game_id;
        this.user_id = user_id;
    }

    public Player(String player_id, String user_name, String bite_code, String is_human, String is_patient_zero, String user_id, String game_id) {
        this.id = player_id;
        this.user_name = user_name;
        this.bite_code = bite_code;
        this.is_human = is_human;
        this.is_patient_zero = is_patient_zero;
        this.user_id = user_id;
        this.game_id = game_id;
    }

    public Player(String username, String bite_code, String is_human, String game_id, String user_id) {

        this.user_name = username;
        this.bite_code = bite_code;
        this.is_human = is_human;
        this.game_id = game_id;
        this.user_id = user_id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getBiteCode() {
        return bite_code;
    }

    public void setBiteCode(String bite_code) {
        this.bite_code = bite_code;
    }

    public String getIs_human() {
        return is_human;
    }

    public void setIs_human(String is_human) {
        this.is_human = is_human;
    }

    public String getIs_patient_zero() {
        return is_patient_zero;
    }

    public void setIs_patient_zero(String is_patient_zero) {
        this.is_patient_zero = is_patient_zero;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getGameId() {
        return game_id;
    }

    public void setGameId(String game_id) {
        this.game_id = game_id;
    }
}
