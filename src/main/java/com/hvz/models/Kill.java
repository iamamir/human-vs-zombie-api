package com.hvz.models;

public class Kill {

    private String id;
    private String time_of_death;
    private String story;
    private String latitude;
    private String longitude;
    private String game_id;
    private String killer_id;
    private String victim_id;

    public Kill(){

    }

    public Kill(String kill_id, String time_of_death, String story, String latitude, String longitude, String game_id, String killer_id, String victim_id) {
        this.id = kill_id;
        this.time_of_death = time_of_death;
        this.story = story;
        this.latitude = latitude;
        this.longitude = longitude;
        this.game_id = game_id;
        this.killer_id = killer_id;
        this.victim_id = victim_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeOfDeath() {
        return time_of_death;
    }

    public void setTimeOfDeath(String time_of_death) {
        this.time_of_death = time_of_death;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGameId() {
        return game_id;
    }

    public void setGameId(String game_id) {
        this.game_id = game_id;
    }

    public String getKillerId() {
        return killer_id;
    }

    public void setKillerId(String killer_id) {
        this.killer_id = killer_id;
    }

    public String getVictimId() {
        return victim_id;
    }

    public void setVictimId(String victim_id) {
        this.victim_id = victim_id;
    }
}
