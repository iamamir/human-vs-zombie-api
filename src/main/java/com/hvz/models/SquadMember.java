package com.hvz.models;

public class SquadMember {
    private String squad_member_id;
    private String rank_;
    private String game_id;
    private String squad_id;
    private String player_id;

    public SquadMember(){

    }
    public SquadMember(String squad_member_id, String rank_, String game_id, String squad_id, String player_id) {
        this.squad_member_id = squad_member_id;
        this.rank_ = rank_;
        this.game_id = game_id;
        this.squad_id = squad_id;
        this.player_id = player_id;
    }

    public String getSquad_member_id() {
        return squad_member_id;
    }

    public void setSquad_member_id(String squad_member_id) {
        this.squad_member_id = squad_member_id;
    }

    public String getRank_() {
        return rank_;
    }

    public void setRank_(String rank_) {
        this.rank_ = rank_;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public String getSquad_id() {
        return squad_id;
    }

    public void setSquad_id(String squad_id) {
        this.squad_id = squad_id;
    }

    public String getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(String player_id) {
        this.player_id = player_id;
    }
}
