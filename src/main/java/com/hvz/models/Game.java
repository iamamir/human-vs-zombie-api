package com.hvz.models;

public class Game {


    private String id;
    private String game_name;
    private String game_state;
    private String nw_latitude;
    private String nw_longitude;
    private String se_latitude;
    private String se_longitude;

    public Game(){

    }

    public Game(String id, String game_name, String game_state, String nw_latitude, String nw_longitude, String se_latitude, String se_longitude) {
        this.id = id;
        this.game_name = game_name;
        this.game_state = game_state;
        this.nw_latitude = nw_latitude;
        this.nw_longitude = nw_longitude;
        this.se_latitude = se_latitude;
        this.se_longitude = se_longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    public String getGame_state() {
        return game_state;
    }

    public void setGame_state(String game_state) {
        this.game_state = game_state;
    }

    public String getNw_latitude() {
        return nw_latitude;
    }

    public void setNw_latitude(String nw_latitude) {
        this.nw_latitude = nw_latitude;
    }

    public String getNw_longitude() {
        return nw_longitude;
    }

    public void setNw_longitude(String nw_longitude) {
        this.nw_longitude = nw_longitude;
    }

    public String getSe_latitude() {
        return se_latitude;
    }

    public void setSe_latitude(String se_latitude) {
        this.se_latitude = se_latitude;
    }

    public String getSe_longitude() {
        return se_longitude;
    }

    public void setSe_longitude(String se_longitude) {
        this.se_longitude = se_longitude;
    }
}
