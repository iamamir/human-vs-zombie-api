# HvZ-API 
### HumanVZombie is a restful application. The application is constructed in Spring and uses SpringSecurity and MySQL to as the main database. The purpose of this API is to adminstrate and play the human vs zombie game. The application implements bearer token authentication, therefore, it's required to first create a user and then log in in order to receive the response token which is used to consume the api endpoints.
### The code is well organized and implemented the needed error handling. Futhermore, API documentation is provided using swagger library. In addition, both the database and the application are deployed and running on Heroku.

### Please note that due to the heroku account is free, the MAX_CONNECTION=10; so it is recommended to download and run on the local host.
#### Here's a link to [swagger documentation on heroku](https://hvz-api-dk.herokuapp.com/swagger-ui/index.html)
#### Here's a link to [Postman collections](https://www.getpostman.com/collections/1eae68527f71eca5893c)
